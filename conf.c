#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "conf.h"

// this code is ugly, but I CBA to rewrite it. it does its job for now.

/*
TODO: add char *section; to confdata and add it next k/v whenever there's a new [section]. only for new ones, though, otherwise keep it NULL.
like this:

x=1	k:x v:1 s:NULL
[s1]	(nothing)
a=2	k:a v:2 s:s1
b=3	k:b v:3 s:NULL
x=4	k:x v:4 s:NULL
*/

size_t config_parseescapes(const char *s) {
	char	*o = s;
	char	*c = s;
	
	char	inq = 0;
	int	cquote = 0;
	
	while (*c) {
		if (!cquote) {
			switch (*c) {
				case '\\':
					cquote = 1;
					break;
				
				case '"':
				case '\'':
					if (inq) {
						if (inq == *c)
							inq = 0;
						else
							*o++ = *c;
					}
					else
						inq = *c;
				
					break;
					
				default:
					*o++ = *c;
			}		
			++c;			
		}		
		else {
			// TODO: some nice \x00 and other stuff like that.
			*o++ = *c++;
			cquote = 0;			
		}		
	}	
	
	return(o - s);
}

void free_configdata(configdata *x) {
	configdata *next;
	
	for (; x != NULL; x = next) {
		next = x->next;
		if (x->key != NULL)
			free(x->key);
		if (x->value != NULL)
			free(x->value);
		free(x);		
	}
}

configdata *read_config(const char * const filename) {
	FILE *f;
	
	f = fopen(filename, "r");
	if (f == NULL) {
		fprintf(stderr, "fopen(%s): %m\n", filename);
		return(NULL);
	}
	
	size_t bufsize = 256;
	ssize_t bufpos = 0;
	char *buf = malloc(bufsize);
	if (buf == NULL) {
		fprintf(stderr, "malloc(): %m\n");
		fclose(f);
		return(NULL);
	}
	
	configdata *ret = NULL;
	configdata *curr = NULL;	
	
	while (1) {
		fgets(buf + bufpos, bufsize - bufpos, f);
		
		size_t l = strlen(buf + bufpos);
		bufpos += l;
		if (bufpos > 0 && (buf[bufpos - 1] == '\n' || feof(f))) {
			if (buf[bufpos - 1] == '\n') {				
				buf[--bufpos] = 0; // clear that \n
				if (bufpos == 0) // empty line
					continue;
				--bufpos;
			}
			
			while (bufpos >= 0 && (buf[bufpos] == '\t' || buf[bufpos] == ' ')) // remove trailing whitespace
				buf[bufpos--] = 0;
			
			if (bufpos < 0) { // line was filled with whitespace only				
				bufpos = 0;
				continue;
			}
			
			char *k;
			for (k = buf; *k && (*k == ' ' || *k == '\t'); ++k); // let's skip leading whitespace in the key
			
			configdata *n = malloc(sizeof(configdata));
			if (n == NULL) {
				fprintf(stderr, "malloc(): %m\n");
				goto fatal;				
			}
			
			char *eq = strchr(k, '=');
			if (eq != NULL) { // it's a key=value
				char *v = eq + 1; // value starts here
				while (*v && (*v == '\t' || *v == ' ')) // except it has leading whitespace, let's clear it
					*v++ = 0;
				
				n->value = strdup(v);
				if (n->value == NULL) {
					fprintf(stderr, "strdup(): %m\n");
					free(n);
					goto fatal;
				}
				
				*eq-- = 0; // this is where the key ends, let's zero the '='
				while (eq > k && (*eq == '\t' || *eq == ' ')) // and again, we have trailing spaces in the key, clear them
					*eq-- = 0;
				
				n->key = strdup(k);
				if (n->key == NULL) {
					fprintf(stderr, "strdup(): %m\n");
					free(n->value);
					free(n);
					goto fatal;
				}				
			}
			else {
				n->key = strdup(k);
				n->value = NULL;				
			}
			n->next = NULL;
			
			if (curr != NULL) { // not the first item
				curr->next = n;
				curr = n;
			}
			else { // first item
				curr = n;
				ret = n;
			}
			
			bufpos = 0;
			*buf = 0;
		}
		else {
			bufsize += 256;
			char *nbuf = realloc(buf, bufsize);
			if (nbuf == NULL) {
				fprintf(stderr, "realloc(): %m\n");
				goto fatal;
			}
			buf = nbuf;
		}
		
		if (feof(f))
			break;
		
	}
	
	free(buf);	
	fclose(f);	
	return(ret);	
	
	fatal:
	free_configdata(ret);
	free(buf);
	fclose(f);
	return(NULL);
}
/*
int main(const int argc, const char *argv[]) {
	if (argc < 2)
		return(1);
	
	configdata *c = read_config(argv[1]);
	
	configdata *n;
	for (n = c; n != NULL; n = n->next)
		printf("key:'%s' value:'%s'\n", n->key, n->value);
	
	free_configdata(c);
	return(0);
}
*/