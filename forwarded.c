#define _GNU_SOURCE
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>

#include <pthread.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <string.h>
#include <time.h>
#include <poll.h>

#include <errno.h>
#include <assert.h>


#include "bwlb.h"

static connlist *connections = NULL;
static connlist *connectionstail = NULL; // just so that we can easily add new connection without traversing from the beginning
static pthread_spinlock_t connections_lock; // and we actually need a lock for this one. this is accessed in the main thread and client threads. i guess this could be changed with non-detached threads.

extern lbdest *destinations;
extern ssize_t destcount;


static pthread_attr_t	forward_thread_attrs; // thread attributes for new forwarding threads

static void lock_connections() {
	pthread_spin_lock(&connections_lock);
}
static void unlock_connections() {
	pthread_spin_unlock(&connections_lock);
}

static int addconnection(conndata *c) {
	connlist *n = malloc(sizeof(connlist));
	if (n == NULL)
		return(-1);
	n->next = NULL;
	n->c = c;

	lock_connections();
	if (connections != NULL) {
		connectionstail->next = n;
		connectionstail = n;
	}
	else {
		connections = n;
		connectionstail = n;
	}
	unlock_connections();

	return(0);
}

static void delconnection(conndata *c) {
	lock_connections();
	connlist *curr, *last = NULL;
	for (curr = connections; curr != NULL; last = curr, curr = curr->next) // if we are absolutely sure c is in the list, we can skip curr != NULL
		if (curr->c == c) {
			if (last != NULL) {
				last->next = curr->next;
			}
			else { // looks like it's head
				connections = curr->next;
			}

			if (curr->next == NULL) // that's tail
				connectionstail = last;

			free(curr);
			break;
		}

	unlock_connections();
}

static void setbandwidth(conndata *c, const uint64_t cnt) {
	// TODO: make this synchronized somehow
	//__sync_set...(&(c->bandwidth[c->lastspeed]), cnt);
	__sync_val_compare_and_swap(&(c->bandwidth[c->lastspeed]), c->bandwidth[c->lastspeed], cnt);
	//c->bandwidth[c->lastspeed] = cnt;

	c->lastspeed++;
	if (c->lastspeed == BANDWIDTHBUFFERSECONDS)
		c->lastspeed = 0;
}

uint64_t getbandwidth(conndata *c) {
	uint64_t	ret = 0;
	int		i;

	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);

	if (tp.tv_sec - c->started > BANDWIDTHBUFFERSECONDS) { // if the connection is longer than BANDWIDTHBUFFERSECONDS, we dont extraploate zeros.
		for (i = 0; i < BANDWIDTHBUFFERSECONDS; ++i)
			ret += __sync_add_and_fetch(&(c->bandwidth[i]), 0);
	}
	else {
		uint64_t	curr;
		int		zeros;

		for (i = 0, zeros = 0; i < BANDWIDTHBUFFERSECONDS; ++i) {
			curr = __sync_add_and_fetch(&(c->bandwidth[i]), 0);
			if (curr == 0)
				++zeros;
			else
				ret += curr;
		}
		// if we had 3 "zeros" reported (no bandwidth), we extrapolate bandwidth (so, for example, 10KB/s achived in 3 secs out of 10 is 10 * 10.0/3)

		if (zeros && zeros < BANDWIDTHBUFFERSECONDS)
			ret *= (BANDWIDTHBUFFERSECONDS * 1.0 / (BANDWIDTHBUFFERSECONDS - zeros));
	}

	return(ret);
}


static void forward_connclose(conndata *c) {
	if (c->srcfd >= 0)
		close(c->srcfd);
	if (c->dstfd >= 0)
		close(c->dstfd);

	delconnection(c);
	free(c);
}

static int open_connection(conndata *c, const int timeout) {
	int fd = socket(c->dest->desthost->ai_family, c->dest->desthost->ai_socktype, c->dest->desthost->ai_protocol);
	if (fd < 0) {
		printf("[" UINT64TPRINTF "] ERROR: socket(): %m\n", c->connid);
		return(-1);
	}

	socket_setnonblock(fd);

	if (connect(fd, c->dest->desthost->ai_addr, c->dest->desthost->ai_addrlen) < 0) {
		if (errno != EINPROGRESS) {
			printf("[" UINT64TPRINTF "] ERROR: connect(): %m\n", c->connid);
			goto errord;
		}
	}

	struct pollfd pfd;
	memset(&pfd, 0, sizeof(struct pollfd));

	pfd.fd = fd;
	pfd.events = POLLOUT;

	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);
	time_t wait_till = tp.tv_sec + timeout;

	while (wait_till > tp.tv_sec) {
		int ret = poll(&pfd, 1, (wait_till - tp.tv_sec) * 100000);
		if (ret < 0) {
			printf("[" UINT64TPRINTF "] ERROR: poll(): %m\n", c->connid);
			close(fd);
			return(-1); // dont mark unhealthy
		}
		if (ret == 0)
			break; // timeout

		if (pfd.revents & POLLOUT) {
			char errstr[256]; // might just alloca(), we return after anyway
			int se;
			socklen_t selen = sizeof(se);
			getsockopt(fd, SOL_SOCKET, SO_ERROR, &se, &selen);
			if (se == 0)
				return(fd);

			printf("[" UINT64TPRINTF "] ERROR: connect(): %s\n", c->connid, strerror_r(se, errstr, sizeof(errstr)));
			goto errord;
		}
		clock_gettime(CLOCK_MONOTONIC, &tp);
	}
	// timeout apparently
	printf("[" UINT64TPRINTF "] ERROR: connect() timed out.\n", c->connid);

	errord:
	close(fd);
	printf("[" UINT64TPRINTF "] Marking %s unhealthy\n", c->connid, c->dest->host);
	clock_gettime(CLOCK_MONOTONIC, &tp);
	c->dest->last_health_check = tp.tv_sec;
	c->dest->unhealthy = 1;
	return(-1);
}

static void *forwardthread3(void *derp) {
	conndata *c = (conndata *)derp;

	int spipes[2], dpipes[2];
	memset(spipes, 0, sizeof(spipes));
	memset(dpipes, 0, sizeof(dpipes));

	if (pipe(spipes) < 0) {
		printf("[" UINT64TPRINTF "] ERROR: pipe(): %m\n", c->connid);
		goto eeeeend;
	}
	if (pipe(dpipes) < 0) {
		printf("[" UINT64TPRINTF "] ERROR: pipe(): %m\n", c->connid);
		goto eeeeend;
	}

	c->dstfd = open_connection(c, c->dstfd);
	if (c->dstfd < 0)
		goto eeeeend;

	socket_setblock(c->dstfd);

	addconnection(c);

	struct pollfd pfds[2];
	memset(&pfds, 0, sizeof(struct pollfd) * 2);

	pfds[0].fd = c->srcfd;
	pfds[1].fd = c->dstfd;
	pfds[0].events = POLLIN;
	pfds[1].events = POLLIN;

	ssize_t to_dst_size = 0;
	ssize_t to_src_size = 0;

	uint64_t currbw = 0;

	struct timespec t, t1;
	clock_gettime(CLOCK_MONOTONIC, &t);
	memcpy(&t1, &t, sizeof(struct timespec));

	while (1) {
		int p = poll(pfds, 2, (1000000000 - t1.tv_nsec) / 1000000);

		if (p < 0)
			goto eeeeend;

		if (pfds[0].revents & POLLOUT) { // we can send to src
			ssize_t len = splice(spipes[0], NULL, c->srcfd, NULL, to_src_size, SPLICE_F_MOVE | SPLICE_F_MORE);
			if (len <= 0) {
				if (len == 0) {
					assert(len != 0);
					break;
				}
				if (errno != EWOULDBLOCK && errno != EAGAIN && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: srv(pipe)->cli splice(): %m\n", c->connid);
					break;
				}
			}
			else {
				to_src_size -= len;
				if (to_src_size == 0)
					pfds[0].events = POLLIN;

				currbw += len;
			}
			pfds[0].revents ^= POLLOUT;
		}

		if (pfds[1].revents & POLLOUT) { // we can send to dst
			ssize_t len = splice(dpipes[0], NULL, c->dstfd, NULL, to_dst_size, SPLICE_F_MOVE | SPLICE_F_MORE);
			if (len <= 0) {
				if (len == 0) {
					assert(len != 0);
					break;
				}
				if (errno != EWOULDBLOCK && errno != EAGAIN && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: cli(pipe)->srv splice(): %m\n", c->connid);
					break;
				}
			}
			else {
				to_dst_size -= len;
				if (to_dst_size == 0)
					pfds[1].events = POLLIN;

				currbw += len;
			}
			pfds[1].revents ^= POLLOUT;
		}

		if (pfds[0].revents & POLLIN) { // we can read from src (and try to send to dst)
			ssize_t len = splice(c->srcfd, NULL, dpipes[1], NULL, 0x7FFFFFFF, SPLICE_F_MOVE | SPLICE_F_MORE);
			if (len <= 0) {
				if (len == 0) {
					printf("[" UINT64TPRINTF "] Connection closed (by CLIENT/%s)\n", c->connid, c->srchost);
					break;
				}
				if (errno != EWOULDBLOCK && errno != EAGAIN && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: cli->srv(pipe) splice(): %m\n", c->connid);
					break;
				}
			}
			else {
				to_dst_size = len; // TODO: to_dst_size is 0 here, maybe jiust ^= ?

				len = splice(dpipes[0], NULL, c->dstfd, NULL, to_dst_size, SPLICE_F_MOVE | SPLICE_F_MORE);
				if (len <= 0) {
					if (len == 0) {
						assert(len != 0);
						break;
					}
					if (errno != EWOULDBLOCK && errno != EAGAIN && errno != EINTR) {
						printf("[" UINT64TPRINTF "] ERROR: cli(pipe)->srv splice(): %m\n", c->connid);
						break;
					}
					pfds[1].events = POLLOUT;
				}
				else {
					to_dst_size -= len;
					if (to_dst_size > 0)
						pfds[1].events = POLLOUT;

					currbw += len;
				}
			}
			pfds[0].revents ^= POLLIN;
		}

		if (pfds[1].revents & POLLIN) { // we can read from dst (and try to send to src)
			ssize_t len = splice(c->dstfd, NULL, spipes[1], NULL, 0x7FFFFFFF, SPLICE_F_MOVE | SPLICE_F_MORE);
			if (len <= 0) {
				if (len == 0) {
					printf("[" UINT64TPRINTF "] Connection closed (by SERVER/%s)\n", c->connid, c->dest->host);
					break;
				}
				if (errno != EWOULDBLOCK && errno != EAGAIN && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: srv->cli(pipe) splice(): %m\n", c->connid);
					break;
				}
			}
			else {
				to_src_size += len; // TODO: to_src_size is 0 here, maybe jiust ^= ?

				len = splice(spipes[0], NULL, c->srcfd, NULL, to_src_size, SPLICE_F_MOVE | SPLICE_F_MORE);
				if (len <= 0) {
					if (len == 0) {
						assert(len != 0);
						break;
					}
					if (errno != EWOULDBLOCK && errno != EAGAIN && errno != EINTR) {
						printf("[" UINT64TPRINTF "] ERROR: srv(pipe)->cli splice(): %m\n", c->connid);
						break;
					}
					pfds[0].events = POLLOUT;
				}
				else {
					to_src_size -= len;
					if (to_src_size > 0)
						pfds[0].events = POLLOUT;

					currbw += len;
				}
			}
			pfds[1].revents ^= POLLIN;
		}

		clock_gettime(CLOCK_MONOTONIC, &t1);
		if (t1.tv_sec > t.tv_sec) { // if we're in the "next" second, we store the amount of data we processed last second.
			setbandwidth(c, currbw); // TODO: if t1.tv_sec - t.tv_sec > 1 we could setbandwidth multiple times... like, for (x = t1.tv_sec - t.tv_sec; x > 0; --x) setbandwidth(c, currbw / (t1.tv_sec - t.tv_sec));
			t.tv_sec = t1.tv_sec;
			currbw = 0; // currbw ^= currbw;
		}
		//t.tv_nsec = t1.tv_nsec; // to make sure we give poll() correct number of ms to sleep (not to sleep too long :-))
	}

	eeeeend:
	close(spipes[0]);
	close(spipes[1]);
	close(dpipes[0]);
	close(dpipes[1]);
	forward_connclose(c);
	return(NULL);
}


static void *forwardthread2(void *derp) {
	conndata *c = (conndata *)derp;

	int spipes[2], dpipes[2];
	memset(spipes, 0, sizeof(spipes));
	memset(dpipes, 0, sizeof(dpipes));

	if (pipe(spipes) < 0) {
		printf("[" UINT64TPRINTF "] ERROR: pipe(): %m\n", c->connid);
		goto eeeeend;
	}
	if (pipe(dpipes) < 0) {
		printf("[" UINT64TPRINTF "] ERROR: pipe(): %m\n", c->connid);
		goto eeeeend;
	}


	c->dstfd = open_connection(c, c->dstfd);
	if (c->dstfd < 0)
		goto eeeeend;

	addconnection(c);

	struct pollfd pfds[2];

	pfds[0].fd = c->srcfd;
	pfds[1].fd = c->dstfd;
	pfds[0].events = POLLIN;
	pfds[1].events = POLLIN;

	struct timespec t, t1;
	clock_gettime(CLOCK_MONOTONIC, &t);

	uint64_t currbw = 0;
	int spliced = 0;
	int its_ok_if_its_zero = 1 | 2;

	while (1) {

		// client -> server
		ssize_t len = splice(c->srcfd, NULL, spipes[1], NULL, 0x7FFFFFFF, SPLICE_F_MOVE | SPLICE_F_MORE | SPLICE_F_NONBLOCK);
		if (len <= 0) {
			if (len == 0) {
				if (!(its_ok_if_its_zero & 1)) {
					printf("[" UINT64TPRINTF "] Connection closed (by CLIENT/%s)\n", c->connid, c->srchost);
					break;
				}
			}
			else {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: splice() (cli->srv(pipe)): %m\n", c->connid);
					break;
				}
				len = 0;
			}
		}

		//printf("[" UINT64TPRINTF "] c->srcfd -> spipes[1]: %d\n", c->connid, len);
		spliced |= len;
		currbw += len;
		while (len > 0) {
			ssize_t x = splice(spipes[0], NULL, c->dstfd, NULL, len, SPLICE_F_MOVE | SPLICE_F_MORE);
			if (x < 0) {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: splice() (cli(pipe)->srv): %m\n", c->connid);
					break;
				}
				x = 0;
			}
			len -= x;
			//printf("[" UINT64TPRINTF "] spipes[0] -> c->dstfd: %d [len: %d]\n", c->connid, x, len);
		}


		// server -> client
		len = splice(c->dstfd, NULL, dpipes[1], NULL, 0x7FFFFFFF, SPLICE_F_MOVE | SPLICE_F_MORE | SPLICE_F_NONBLOCK);
		if (len <= 0) {
			if (len == 0) {
				if (!(its_ok_if_its_zero & 2)) {
					printf("[" UINT64TPRINTF "] Connection closed (by SERVER/%s)\n", c->connid, c->dest->host);
					break;
				}
			}
			else {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: splice() (srv->cli(pipe)): %m\n", c->connid);
					break;
				}
				len = 0;
			}
		}


		its_ok_if_its_zero |= 3;

		//printf("[" UINT64TPRINTF "] c->dstfd -> dpipes[1]: %d\n", c->connid, len);

		spliced |= len;
		currbw += len;
		while (len > 0) {
			ssize_t x = splice(dpipes[0], NULL, c->srcfd, NULL, len, SPLICE_F_MOVE | SPLICE_F_MORE);
			if (x < 0) {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					printf("[" UINT64TPRINTF "] ERROR: splice() (srv(pipe)->cli): %m\n", c->connid);
					break;
				}
				x = 0;
			}
			len -= x;
			//printf("[" UINT64TPRINTF "] dpipes[0] -> c->srcfd: %d [len: %d]\n", c->connid, x, len);
		}

		clock_gettime(CLOCK_MONOTONIC, &t1);
		if (t1.tv_sec > t.tv_sec) { // if we're in the "next" second, we store the amount of data we processed last second.
			setbandwidth(c, currbw); // TODO: if t1.tv_sec - t.tv_sec > 1 we could setbandwidth multiple times... like, for (x = t1.tv_sec - t.tv_sec; x > 0; --x) setbandwidth(c, currbw / (t1.tv_sec - t.tv_sec));
			t.tv_sec = t1.tv_sec;
			currbw = 0; // currbw ^= currbw;
		}
		//t.tv_nsec = t1.tv_nsec; // to make sure we give poll() correct number of ms to sleep (not to sleep too long :-))

		if (!spliced) {
			//printf("[" UINT64TPRINTF "] poll()\n", c->connid);
			int ret = poll(pfds, 2, (1000000000 - t1.tv_nsec) / 1000000);
			if (ret < 0) {
				printf("[" UINT64TPRINTF "] ERROR: poll(): %m\n", c->connid);
				break;
			}
			//printf("pfds[0].revents: %d   pfds[1].revents: %d\n", pfds[0].revents, pfds[1].revents);

			if (pfds[0].revents & POLLIN)
				its_ok_if_its_zero ^= 1;
			if (pfds[1].revents & POLLIN)
				its_ok_if_its_zero ^= 2;
		}
		else
			spliced = 0;
	}

	eeeeend:
	close(spipes[0]);
	close(spipes[1]);
	close(dpipes[0]);
	close(dpipes[1]);
	forward_connclose(c);
	return(NULL);
}



/*
if we have no splice...
static int64_t processdata(int readablefd, conndata *c) {
	static	buf[65536];
	ssize_t len;

	len = recv(readablefd, buf, sizeof(buf));
	if (len <= 0)
		return(len);

	switch (readablefd) {
		case c->srcfd:
			if (send(c->dstfd, buf, len, MSG_NOPIPE) < len)
				return(-1);
			break;
		default: //dstfd
			if (send(c->srcfd, buf, len, MSG_NOPIPE) < len)
				return(-1);
	}
	return(len);
}
*/
static ssize_t processdata(const int rfd, const int wfd, int pipes[2]) {
/*	int len;

	if (ioctl(rfd, FIONREAD, &len) < 0) // or SIOCINQ
		return(-1);
*/
	// TODO: possible to splice too much and pipe write will block? need to find out the max splice size, perhaps. or was this bug fixed?
	ssize_t len = splice(rfd, NULL, pipes[1], NULL, 0x7FFFFFFF, SPLICE_F_MOVE | SPLICE_F_MORE);
	if (len < 0) {
		return(-1);
	}
	ssize_t w = len;

	while (w > 0) {
		ssize_t x = splice(pipes[0], NULL, wfd, NULL, w, SPLICE_F_MOVE | SPLICE_F_MORE);
		if (x < 0) {
			return(-1);
		}
		w -= x;
	}

	return(len);
}

static void *forwardthread(void *derp) {
	conndata *c = (conndata *)derp;

	printf("There's no reason to use forwardthread() over forwardthread3(), the latter is faster.\n");

	int pipes[2];

	if (pipe(pipes) < 0) {
		printf("[" UINT64TPRINTF "] ERROR: pipe(): %m\n", c->connid);
		forward_connclose(c);
		return(NULL);
	}

	c->dstfd = open_connection(c, c->dstfd);
	if (c->dstfd < 0) {
		close(pipes[0]);
		close(pipes[1]);
		forward_connclose(c);
		return(NULL);
	}

	socket_setblock(c->dstfd);

	addconnection(c);

	struct pollfd pfds[2];

	pfds[0].fd = c->srcfd;
	pfds[1].fd = c->dstfd;
	pfds[0].events = POLLIN;
	pfds[1].events = POLLIN;

	struct timespec t, t1;
	clock_gettime(CLOCK_MONOTONIC, &t);
	memcpy(&t1, &t, sizeof(struct timespec));

	uint64_t currbw = 0;
	while (1) {
		int ret = poll(pfds, 2, (1000000000 - t1.tv_nsec) / 1000000);
		int64_t processed;

		// process fds if something was read
		if (ret > 0) {
			if (pfds[0].revents & POLLIN) {
				processed = processdata(c->srcfd, c->dstfd, pipes);
				if (processed <= 0) {
					if (processed < 0)
						printf("[" UINT64TPRINTF "] ERROR: cli->srv splice(): %m\n", c->connid);
					else
						printf("[" UINT64TPRINTF "] Connection closed (by CLIENT/%s)\n", c->connid, c->srchost);
					break;
				}
				currbw += processed;
			}
			if (pfds[1].revents & POLLIN) {
				processed = processdata(c->dstfd, c->srcfd, pipes);
				if (processed <= 0) {
					if (processed < 0)
						printf("[" UINT64TPRINTF "] ERROR: srv->cli splice(): %m\n", c->connid);
					else
						printf("[" UINT64TPRINTF "] Connection closed (by SERVER/%s)\n", c->connid, c->dest->host);
					break;
				}
				currbw += processed;
			}
		}

		clock_gettime(CLOCK_MONOTONIC, &t1);
		if (t1.tv_sec > t.tv_sec) { // if we're in the "next" second, we store the amount of data we processed last second.
			setbandwidth(c, currbw); // TODO: if t1.tv_sec - t.tv_sec > 1 we could setbandwidth multiple times... like, for (x = t1.tv_sec - t.tv_sec; x > 0; --x) setbandwidth(c, currbw / (t1.tv_sec - t.tv_sec));
			t.tv_sec = t1.tv_sec;
			currbw = 0; // currbw ^= currbw;
		}
		//t.tv_nsec = t1.tv_nsec; // to make sure we give poll() correct number of ms to sleep (not to sleep too long :-))
	}
	close(pipes[0]);
	close(pipes[1]);
	forward_connclose(c);
	return(NULL);
}

void get_bandwidths(uint64_t *dest) {
	memset(dest, 0, sizeof(uint64_t) * destcount);

	lock_connections();
	connlist *c;
	for (c = connections; c != NULL; c = c->next) {
		int i = c->c->dest - destinations; // yay pointer arithmetic
		dest[i] += getbandwidth(c->c);
	}
	unlock_connections();
}




static uint64_t connid = 0;
int new_forwarded_connection(const int fd, const char * const src, const size_t destnum, const int timeout) {
	conndata *n = malloc(sizeof(conndata));
	if (n == NULL) {
		close(fd);
		return(-1);
	}

	memset(n, 0, sizeof(conndata));
	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);

	n->started = tp.tv_sec;
	n->connid = connid++;
	n->srcfd = fd;
	n->dstfd = timeout; // TODO: terrible way to do this. fix it.

	socket_setnonblock(fd);
	//sockaddr_to_char(n->srchost, src);
	memcpy(n->srchost, src, MAXADDRLEN);
	printf("[" UINT64TPRINTF "] New connection from %s\n", n->connid, n->srchost);

	n->dest = &(destinations[destnum]);
	printf("[" UINT64TPRINTF "] Destination: %s\n", n->connid, destinations[destnum].host);

	// start the forward thread for this connection
	pthread_t t;
	if (pthread_create(&t, &forward_thread_attrs, &forwardthread3, (void *)n) != 0) {
		printf("[" UINT64TPRINTF "] ERROR: pthread_create(): %m\n", n->connid);
		forward_connclose(n);
		return(-1);
	}

	return(0);
}

char *get_conns_str(size_t *rlen) {
	size_t len = 0;

	lock_connections();
	connlist *c;

	int conncnt = 0;
	for (c = connections; c != NULL; c = c->next, ++conncnt);
	uint64_t connspeed[conncnt];

	int conni;
	for (c = connections, conni = 0; c != NULL; c = c->next, ++conni) {
		int i;
		connspeed[conni] = 0;
		for (i = 0; i < BANDWIDTHBUFFERSECONDS; ++i) // TODO: like in getbandwidth(), extrapolate zeros if the connection is fresh enough (i.e. not all BANDWIDTHBUFFERSECONDS buckets were filled)
			connspeed[conni] += c->c->bandwidth[i];

		i = c->c->dest - destinations; // yay pointer arithmetic
		len += numlen(c->c->connid) + 1 + strlen(c->c->srchost) + 1 + strlen(destinations[i].host) + 1 + numlen(connspeed[conni]) + 1;
	}

	len += 2;
	len += strlen("+STATS\n");
	char *ret = malloc(len);
	if (ret == NULL)
		goto end;

	memcpy(ret, "+STATS\n", strlen("+STATS\n"));
	char *cret = ret + strlen("+STATS\n");

	for (c = connections, conni = 0; c != NULL; c = c->next, ++conni) {
		int i = c->c->dest - destinations; // yay pointer arithmetic
		cret += sprintf(cret, UINT64TPRINTF " %s %s " UINT64TPRINTF "\n", c->c->connid, c->c->srchost, destinations[i].host, connspeed[conni]);
	}
	*cret++ = '.';
	*cret = '\n';
	*rlen = len;

	end:
	unlock_connections();
	return(ret);
}

int shutdown_connection_nr(const uint64_t cnr) {
	lock_connections();
	int ret = -1;

	connlist *c;
	for (c = connections; c != NULL; c = c->next) {
		if (cnr == c->c->connid) {
			// c->c->force_terminated = 1; ?
			shutdown(c->c->srcfd, SHUT_RDWR);
			ret = 0;
			break;
		}
	}

	unlock_connections();
	return(ret);
}


static void init_stuff(void) __attribute__((constructor));
static void destroy_stuff(void) __attribute__((destructor));


static void init_stuff() {
	assert(pthread_spin_init(&connections_lock, PTHREAD_PROCESS_PRIVATE) == 0);

	assert(pthread_attr_init(&forward_thread_attrs) == 0);
	// pthread_attr_setstacksize(&forward_thread_attrs, 128 * 1024);
	assert(pthread_attr_setdetachstate(&forward_thread_attrs, PTHREAD_CREATE_DETACHED) == 0);
}

static void destroy_stuff() {
	pthread_attr_destroy(&forward_thread_attrs);

	assert(pthread_spin_destroy(&connections_lock) == 0);
}
