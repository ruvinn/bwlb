CFLAGS = -O0 -Wall -g
LDFLAGS = -pthread

all: bwlb

clean:
	rm -f bwlb bwlb.o conf.o forwarded.o

bwlb.o: bwlb.c bwlb.h
	$(CC) -c -o bwlb.o bwlb.c $(CFLAGS)

forwarded.o: forwarded.c bwlb.h
	$(CC) -c -o forwarded.o forwarded.c $(CFLAGS)

conf.o: conf.c conf.h
	$(CC) -c -o conf.o conf.c $(CFLAGS)

bwlb: bwlb.o conf.o forwarded.o
	$(CC) -o bwlb bwlb.o conf.o forwarded.o $(LDFLAGS)
