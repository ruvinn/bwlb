# bwlb

BandWidth-aware Load Balancer

The idea is to:
 - load a list of destinations,
 - listen on defined host:port as a client endpoint(1),
 - listen on defined host:port as an admin/other node endpoint(2),
 - when something connects to our incoming socket(1), we check the bandwidth for each destination by adding bandwidth for each connection that goes to given destination; our new connection goes to dest with lowest bandwidth -> loadbalancing by bandwidth.
 - we use splice() to make sure we don't copy kernelspace->userspace->kernelspace. we don't care about the content being sent.
 - give the ability to control the connections by user (basically means killing ongoing connections, changing endpoint/destination status, etc)

How to use:
After compiling, create a config file (name is irrelevant, you pass it as first argument to bwlb), for example:
```
node_name      = lbhost1:11111
network        = lbhost1:11111,lbhost2:11111,lbhost3:11111
listen_addr    = localhost:8080
control_addr   = lbhost1:11111
destinations   = somehost1:123,somehost2:321,somehost3:213,somehost4:312

node_ping_timeout           = 15
maintenance_checks_interval = 15
bwstats_max_delay           = 5
dest_connection_timeout     = 5
```

Explanation of options:
  - listen_addr - this is our host:port to bind to for connections we forward to defined destinations
  - destinations - a comma-separated list of host:port of destinations we forward connections to
  - control_addr - this is a host:port we bind our control/node socket to. make sure it's reachable by other nodes in the network
  - network - comma-separated list of all the nodes (bwlbs) in our network. those are actual host:port addresses we need to be able to reach
  - node_name - our node name, should be one of those defined in `network`; we use it to advertise ourselves to other nodes (so if we tell them we're X, they won't be trying to connect to us if they have us listed in their `network`), also ignore it in our own `network` list (so that we don't try to connect to ourselves)
  - node_ping_timeout - if no communication with peer bwlb node happened in this time, we assume the connection is broken [seconds]
  - maintenance_checks_interval - how often we check and try to reconnect to other bwlb nodes and re-check destinations marked as unhealthy [seconds]
  - bwstats_max_delay - how long to wait for response to bandwidth stats from other bwlb nodes before assuming they won't reply (this is to make sure we don't wait forever, also not to delay forwarding the new connection to some destination for longer than this) [seconds]
  - dest_connection_timeout - connect() timeout for destination, if we don't successfully connect in this time, we assume the destination is down (and mark it as unhealthy) [seconds]

Given this design, the way we deploy that config for all the nodes should be simple - all we need to change is `node_name`, no reason to touch `network` for each node we put it on.

After starting them up, they should try connecting to eachother (each connects to all others defined in `network`, so we always have the fastest direct path to each node + no issues if some of them go down).
When a successful connection is established, nodes ping eachother every `node_ping_timeout` seconds (unless other communication was sent there before this time is reached, which resets the counter). if still no response in `node_ping_timeout`, nodes try to reconnect.
Whenever a new connection is established by client to `listen_addr`, the node which received the connection asks all other nodes for their bandwidth stats and computes which destination should receive current connection - and then forwards it there.

Commands available for the user (issue after connecting to control_addr):  
**BWSTAT**  
**BWSTATS**  
lists destinations defined along with the current bandwidth connections going through this node send/receive (sum) to them

**STAT**  
**STATS**  
lists all client connections to current node with their id, source, destination and bandwidth

**KICK** *[id]*  
**DISCONNECT** *[id]*  
force-closes client connection with id *[id]* (obtained through **STAT**/**STATS**)

**DISABLE** *[destname]*  
disables destination *[destname]* - it won't be considered healthy for new connections

**ENABLE** *[destname]*  
re-enables disabled destination *[destname]*

**HEALTHY** *[destname]*  
**MARKHEALTHY** *[destname]*  
marks destination *[destname]* as healthy; usually this is done automatically if the destination is up, checked every `maintenance_checks_interval` seconds

**UNHEALTHY** *[destname]*  
**MARKUNHEALTHY** *[destname]*  
marks given destination *[destname]* as unhealthy. new connections won't connect there. however, the difference between this and disabling a destination is that unhealthy destinations are re-enabled automatically if they are up (checked every `maintenance_cheks_interval`),
therefore it's unlikely you want to use this command.

**DESTS**  
**LISTDESTS**  
**DESTINATIONS**  
**LISTDESTINATIONS**  
lists all defined destinations on a current node along with their status (U - unhealthy, D - disabled)

**PING**  
just responds with PONG


Be aware that all the status data is returned (and actually used for) given node only. If you disable destination on one node, other nodes will still attempt to send clients there.
Moreover, if a destination is UNHEALTHY on one node, other nodes will still try to connect there (it's possible that only one node has issues connecting there, hence it should not prevent the whole network from trying).