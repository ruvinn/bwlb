#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

typedef struct __bwlbconfig {
	char	*node_name;

	char	**other_nodes;

	char	*listen_addr, *control_addr;
	int		node_ping_timeout;
	int		maintenance_checks_interval;
	int		bwstats_max_delay;
	int		unhealthy_checks_interval;
	int		dest_connection_timeout;
} bwlbconfig;


#define BANDWIDTHBUFFERSECONDS 10 // how many seconds we keep for the bandwidth computation. it's a moving window thing, always updating the oldest entry, so the sum of those fields always tells us the correct amount of data transferred in the last BANDWIDTHBUFFERSECONDS seconds.
#define UINT64TPRINTF "%lu"
#define MAXADDRLEN (INET6_ADDRSTRLEN + 6)
/* for each destination host we just need it resolved and the original name (from config, human-readable, easy to recognize) to print and compare when getting data from other nodes */
typedef struct __lbdest {
	struct addrinfo *desthost;

	char	*host;

	int		unhealthy:1;
	int		disabled:1;
	time_t	last_health_check;
} lbdest;


typedef struct __conndata {
	lbdest		*dest; // this client connects to this dest
	char		srchost[MAXADDRLEN];

	int srcfd, dstfd; // client socket, destination socket

	time_t		started; // when the connection started... not used yet.
	uint64_t	connid; // unique connection id just for logging

	uint64_t	bandwidth[BANDWIDTHBUFFERSECONDS]; // ring buffer of last BANDWIDTHBUFFERSECONDS bandwidth measurements.; these are accessed from multiple threads, therefore make it sync.
	int		lastspeed; // ringbuffer pos.
} conndata;

/* linked list of connections. if we're gonna have lots of connections, may be a good idea to have a hash table by source fd */
typedef struct __connlist {
	conndata		*c;

	struct __connlist	*next;
} connlist;


#define BUFCHUNK 256
#define MAXINCOMINGBUF 512 // i mean, come on, we dont need more than this

enum cli_types { // this goes to clidescr->cli_type and defines what kind of struct we have as clidescr->cli_data.
	TYPE_LISTEN, // forward listen socket. clients that get "loadbalanced"
	TYPE_CONTROLLISTEN, // control listen socket, we get connections from other nodes and text clients (admins) here
	TYPE_NOIDEA, // client did not send anything yet. we're waiting - if the first command is a handshake, then we change to OTHERNODE. otherwise CONTROLCONN i guess
	TYPE_OTHERNODE, // other node connected to us
	TYPE_OTHERNODECONNECTING, // we are connecting to that other node (so basically check result on POLLOUT)
	TYPE_OTHERNODEOUT, // we connected to that other node
	TYPE_CONTROLCONN, // control connection from human
	TYPE_HEALTHCHECK // just a healthcheck connection to destination - if it succeeds, we just close it and mark dest as healthy
};

enum node_states {
	NODE_IDLE,
	NODE_INBWSTATS
};

typedef struct __cli_othernode {
	char		*node_name;
	int			state;
	uint64_t	*bwstats;

	time_t		lasttransmission;

	int			sent_bwstats:1;
	int			sent_ping:1;
} cli_othernode;

/*typedef struct __cli_default {

} cli_default;*/


typedef struct __clidescriptor {
	int		cli_type;

	char	srchost[MAXADDRLEN];

	char	*rbuf, *sbuf; // everyone needs a recv/send buffer
	size_t	rbufsize, sbufsize;
	size_t	rbufpos, sbufpos;

	void	*cli_data; // or maybe change it to an union?
} clidescr;

// helper macros to cast clidescr->cli_data to struct for easy acces. CLIDATA_OTHERNODE(x)->state for example.
#define CLIDATA_OTHERNODE(x) ((cli_othernode *)(clis[x].cli_data))
#define CLIDATA_DEFAULT(x) ((cli_default *)(clis[x].cli_data))
// helper macros to quickly check type of the client. IS_OTHERNODECONNECTED means we have an established connection and can talk to the other node.
#define IS_OTHERNODECONNECTED(x) (clis[x].cli_type == TYPE_OTHERNODE || clis[x].cli_type == TYPE_OTHERNODEOUT)
#define IS_OTHERNODE(x) (clis[x].cli_type == TYPE_OTHERNODE || clis[x].cli_type == TYPE_OTHERNODEOUT || clis[x].cli_type == TYPE_OTHERNODECONNECTING)


#define NODE_GREETING "YHELOTHERE:3" // just a command for inter-node announcement, basically something a human should not type.
#define NODE_GREETING_LEN (strlen(NODE_GREETING)) // lazy


void sockaddr_to_char(char *, const struct sockaddr *);
int numlen(const uint64_t);
int socket_setnonblock(int);
int socket_setblock(int);
void get_bandwidths(uint64_t *);
char *get_conns_str(size_t *);
int new_forwarded_connection(const int, const char * const, const size_t, const int);
int shutdown_connection_nr(const uint64_t);
