typedef struct __configdata {
	char	*key, *value;
	int	line;
	
	struct __configdata	*next;
} configdata;

void free_configdata(configdata *x);
configdata *read_config(const char * const filename);
size_t config_parseescapes(const char *s);
