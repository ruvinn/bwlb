#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>


#include <assert.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <string.h>
#include <time.h>
#include <poll.h>

#include <signal.h>
#include <errno.h>


#include <stdio.h>

#include "bwlb.h"
#include "conf.h"

// TODO: cleanup headers after split

// this is probably the first time i actually give up full cleaning on exit (for those valgrind points, you know)
// well, technically this does NOT exit normally...

lbdest	*destinations = NULL; // we're not gonna be modifying this too often, so it's fine as an array
ssize_t	destcount = 0;

static bwlbconfig config;

static ssize_t pfdsize = 16;
static ssize_t pfdcount = 2;
static struct pollfd *pfd = NULL;
static clidescr *clis = NULL;

static int *forward_fds = NULL;
static char *forward_srcs = NULL;
static size_t forward_fd_count = 0;
static size_t forward_fd_size = 0;
static int bwstat_requests_in_flight = 0;

static time_t next_maintenance_checks = 0;
static time_t next_connection_forward_time = 0;

/*

	idea:
	we could have a thread that will literally just do:
	static int timecounter;
	void threadproc(void *) {
		struct timespec tp;
		time_t		l;

		clock_gettime(CLOCK_MONOTONIC, &tp);
		l = tp.tv_sec;

		while (1) {
			usleep(1000000 - tp.tv_usec);
			clock_gettime(CLOCK_MONOTONIC, &tp);
			if (tp.tv_sec != l) {
				timecounter += tp.tv_sec - l;
				l = tp.tv_sec;
			}
		}
	}
	if clock_gettime() contextswitches, then one thread can do that instead of all connections.
	but... nope: http://oliveryang.net/2015/09/pitfalls-of-TSC-usage/
*/




static ssize_t destination_get_index(const char * const name, const size_t len) {
	ssize_t ret;

	for (ret = 0; ret < destcount; ++ret) {
		if (strlen(destinations[ret].host) == len && !strncmp(destinations[ret].host, name, len))
			return(ret);
	}

	return(-1);

}

void sockaddr_to_char(char *ret, const struct sockaddr *s) { // must be thread-safe
	int len;

	switch (s->sa_family) {
		case AF_INET:
			inet_ntop(AF_INET, &(((struct sockaddr_in *)s)->sin_addr), ret, MAXADDRLEN);
			len = strlen(ret);
			snprintf(ret + len, MAXADDRLEN - len - 1, ":%d", ((struct sockaddr_in *)s)->sin_port);
			break;

		case AF_INET6:
			inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)s)->sin6_addr), ret, MAXADDRLEN);
			len = strlen(ret);
			snprintf(ret + len, MAXADDRLEN - len - 1, ":%d", ((struct sockaddr_in6 *)s)->sin6_port);
			break;

		case AF_UNIX:
		default:
			*ret = 0;
	}
}

struct addrinfo *resolve(const char *host) {
	char *chost = strdup(host);
	if (chost == NULL)
		return(NULL);

	char *cport = strrchr(chost, ':');
	if (cport != NULL)
		*cport++ = 0;

	struct addrinfo	hints;
	struct addrinfo *res = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;

	int err = getaddrinfo(chost, cport, &hints, &res);
	free(chost);

	if (err != 0)
		return(NULL);

	return(res);
}

int socket_setnonblock(int fd) {
        int flags = fcntl(fd, F_GETFL);
		if (flags < 0)
			return(-1);
        flags |= O_NONBLOCK;
        return(fcntl(fd, F_SETFL, flags));
}

int socket_setblock(int fd) {
        int flags = fcntl(fd, F_GETFL);
		if (flags < 0)
			return(-1);
        flags &= ~O_NONBLOCK;
        return(fcntl(fd, F_SETFL, flags));
}

int socket_listenon(const char *addr, const int nonblocking) {
	struct addrinfo *res = resolve(addr);
	assert(res != NULL);

	int listensock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	assert(listensock >= 0);

	int x = 1;
	if (setsockopt(listensock, SOL_SOCKET, SO_REUSEADDR, &x, sizeof(x)) == -1) {
		perror("setsockopt(SO_REUSEADDR)");
	}

	assert(bind(listensock, res->ai_addr, res->ai_addrlen) == 0);
	freeaddrinfo(res);

	if (listen(listensock, SOMAXCONN) != 0) {
		close(listensock);
		return(-1);
	}

	if (nonblocking)
		if (socket_setnonblock(listensock) != 0) {
			close(listensock);
			return(-1);
		}

	return(listensock);
}

int socket_connectto(const struct addrinfo *addr, const int nonblocking) {
	int sock = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
	if (sock < 0) {
		perror("socket()");
		return(-1);
	}

	int x = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &x, sizeof(x)) == -1) {
		perror("setsockopt(SO_REUSEADDR)");
	}

	if (nonblocking)
		socket_setnonblock(sock);

	if (connect(sock, addr->ai_addr, addr->ai_addrlen) < 0) {
		if (errno != EINPROGRESS) {
			perror("connect()");
			close(sock);
			return(-1);
		}
		if (!nonblocking) {
			perror("connect()");
			close(sock);
			return(-1);
		}
	}

	return(sock);
}

int numlen(const uint64_t n) { // TODO: this... needs to be done differently or at least optimized (so that we hit the most common values earlier). not that it matters, but it's ugly otherwise. well, it's ugly anyway.
	if (n >	9999999999999999999U) return(20);
	if (n >	999999999999999999U) return(19);
	if (n >	99999999999999999U) return(18);
	if (n >	9999999999999999U) return(17);
	if (n >	999999999999999U) return(16);
	if (n >	99999999999999U) return(15);
	if (n >	9999999999999U) return(14);
	if (n >	999999999999U) return(13);
	if (n >	99999999999U) return(12);
	if (n >	9999999999U) return(11);
	if (n >	999999999U) return(10);
	if (n >	99999999U) return(9);
	if (n >	9999999U) return(8);
	if (n >	999999U) return(7);
	if (n >	99999U) return(6);
	if (n >	9999U) return(5);
	if (n >	999U) return(4);
	if (n >	99U) return(3);
	if (n >	9U) return(2);
	return(1);
}

static char *get_bandwidths_str(size_t *rlen) {
	uint64_t bws[destcount];

	get_bandwidths(bws);

	int i;
	size_t len = 0;

	for (i = 0; i < destcount; ++i)
		len += strlen(destinations[i].host) + 1 + numlen(bws[i]) + 1;

	len += 2;
	len += strlen("+BWSTATS\n");

	char *ret = malloc(len);
	if (ret == NULL)
		return(NULL);

	memcpy(ret, "+BWSTATS\n", strlen("+BWSTATS\n"));
	char *retp = ret + strlen("+BWSTATS\n");
	for (i = 0; i < destcount; ++i)
		retp += sprintf(retp, "%s " UINT64TPRINTF "\n", destinations[i].host, bws[i]);
	*retp++ = '.';
	*retp = '\n';
	*rlen = len;
	return(ret);
}

static char *get_dests_str(size_t *rlen) {
	int i;
	size_t len = 0;

	for (i = 0; i < destcount; ++i)
		len += strlen(destinations[i].host) + (destinations[i].unhealthy || destinations[i].disabled ? 1 : 0) + (destinations[i].unhealthy ? 1 : 0) + (destinations[i].disabled ? 1 : 0) + 1;
		// host + " " (if unhealthy or disabled) + "U" (if unhealthy) + "D" (if disabled) + \n
	len += 2; // .\n at the end
	len += strlen("+DESTS\n"); // at the beginning

	char *ret = malloc(len);
	if (ret == NULL)
		return(NULL);

	memcpy(ret, "+DESTS\n", strlen("+DESTS\n"));
	char *retp = ret + strlen("+DESTS\n");
	for (i = 0; i < destcount; ++i)
		retp += sprintf(retp, "%s%s%s%s\n", destinations[i].host, (destinations[i].unhealthy || destinations[i].disabled ? " " : ""), (destinations[i].unhealthy ? "U" : ""), (destinations[i].disabled ? "D" : ""));

	*retp++ = '.';
	*retp = '\n';
	*rlen = len;
	return(ret);
}



//static int cli_send(const int fd, clidescr *cli, const char * const buf, size_t len) {
static int cli_send(const ssize_t cli, const char * const buf, size_t len) {
	// TODO: those bufsize computations are a bit wasteful if len equals BUFCHUNK. in that case we allocate one full BUFCHUNK more. for example, if len == BUFCHUNK then len/BUFCHUNK == 1 but we allocate 2 * BUFCHUNK.
	// should really be ceil(len / BUFCHUNK) * BUFCHUNK

	if (likely(clis[cli].sbuf == NULL)) { // we have no backlog.
		ssize_t sent = send(pfd[cli].fd, buf, len, MSG_NOSIGNAL);
		if (likely(sent == len))
			return(0); // sent everything

		if (unlikely(sent < 0))
			return(-1); // error

		len -= sent;

		clis[cli].sbufsize = (len / BUFCHUNK + 1) * BUFCHUNK;
		clis[cli].sbuf = malloc(clis[cli].sbufsize);
		if (unlikely(clis[cli].sbuf == NULL)) {
			clis[cli].sbufsize = 0;
			return(-1); // error
		}
		memcpy(clis[cli].sbuf, buf + sent, len);
		clis[cli].sbufpos = len;

		return(1); // we have backlog
	}

	// we have backlog, sadly
	if (clis[cli].sbufpos + len > clis[cli].sbufsize) { // it wont fit... need to enlarge buffer again
		size_t ns = ((clis[cli].sbufpos + len) / BUFCHUNK + 1) * BUFCHUNK;
		char *nb = realloc(clis[cli].sbuf, ns);
		if (unlikely(nb == NULL))
			return(-1);

		clis[cli].sbuf = nb;
		clis[cli].sbufsize = ns;
	}

	memcpy(clis[cli].sbuf + clis[cli].sbufpos, buf, len); // and just append our buf to the end
	clis[cli].sbufpos += len;
	return(0); // we STILL have backlog
}

//static int cli_flush(const int fd, clidescr *cli) {
static int cli_flush(const ssize_t cli) {
	ssize_t sent = send(pfd[cli].fd, clis[cli].sbuf, clis[cli].sbufpos, MSG_NOSIGNAL);
	if (unlikely(sent < 0))
		return(-1); // error

	clis[cli].sbufpos -= sent;
	if (clis[cli].sbufpos > 0) {
		memmove(clis[cli].sbuf, clis[cli].sbuf + sent, clis[cli].sbufpos);
		return(0); // we still have remaining data
	}
	free(clis[cli].sbuf);
	clis[cli].sbufsize = 0;
	return(1); // we flushed everything \o/
}


static int malloc_more_clis(const int fd) {
	ssize_t npfdsize = pfdsize * 2; // TODO: might want to make this logarythmic, especially given nfile limits

	struct pollfd *npfd = realloc(pfd, sizeof(struct pollfd) * npfdsize);
	if (npfd == NULL) {
		printf("[C%d] ERROR: realloc(): %m\n", fd);
		return(-1);
	}

	pfd = npfd;

	clidescr *nclis = realloc(clis, sizeof(clidescr) * npfdsize);
	if (nclis == NULL) {
		printf("[C%d] ERROR: realloc(): %m\n", fd);
		return(-1); // there's technically no harm in leaving pfd as is here.
	}

	clis = nclis;
	pfdsize = npfdsize; // do it here, because if we do it above if nclis realloc fails, pfdsize will increase even though it wont cover new size of clis.
	return(0);
}


static void cli_disconnect(const ssize_t cli) {
	close(pfd[cli].fd);

	if (clis[cli].cli_data != NULL) {
		switch (clis[cli].cli_type) {
			case TYPE_OTHERNODE: // those have cli_othernode
			case TYPE_OTHERNODEOUT:
			case TYPE_OTHERNODECONNECTING:
				if (CLIDATA_OTHERNODE(cli)->node_name != NULL)
					free(CLIDATA_OTHERNODE(cli)->node_name);

				if (CLIDATA_OTHERNODE(cli)->bwstats != NULL)
					free(CLIDATA_OTHERNODE(cli)->bwstats);

				if (CLIDATA_OTHERNODE(cli)->state == NODE_INBWSTATS || CLIDATA_OTHERNODE(cli)->sent_bwstats) {
					if (--bwstat_requests_in_flight == 0)
							next_connection_forward_time = 1;
				}
				free(clis[cli].cli_data);
				break;

			case TYPE_HEALTHCHECK: // this one does not have anything alloced there
				break;

			default: // everything else possibly has something.
				free(clis[cli].cli_data);
		}
	}

	if (clis[cli].sbuf != NULL)
		free(clis[cli].sbuf);

	if (clis[cli].rbuf != NULL)
		free(clis[cli].rbuf);


	--pfdcount;
	if (cli < pfdcount) {
		memcpy(&(clis[cli]), &(clis[pfdcount]), sizeof(clidescr));
		memcpy(&(pfd[cli]), &(pfd[pfdcount]), sizeof(struct pollfd));
	}

	// now check if we actually have fewer connections than we have allocated space for / 4. If yes, shrink the struct by 50%.
	if (pfdcount < pfdsize / 4) {
		int npfdsize = pfdsize / 2;

		struct pollfd *npfd = realloc(pfd, sizeof(struct pollfd) * npfdsize);
		if (npfd == NULL)
			return;

		pfd = npfd;
		pfdsize = npfdsize;

		clidescr *nclis = realloc(clis, sizeof(clidescr) * npfdsize);
		if (nclis == NULL)
			return;

		clis = nclis;
	}
}


static ssize_t new_cli(const int fd, const int cli_type) {
	if (pfdcount == pfdsize)
		if (malloc_more_clis(fd) < 0)
			return(-1);

	memset(&(clis[pfdcount]), 0, sizeof(clidescr));

	pfd[pfdcount].fd = fd;
	pfd[pfdcount].revents = 0;

	clis[pfdcount].cli_type = cli_type;
	switch (cli_type) {
		case TYPE_OTHERNODE:  // POLLIN in events, cli_data is cli_othernode
		case TYPE_OTHERNODEOUT:
			pfd[pfdcount].events = POLLIN;
			break;

		case TYPE_OTHERNODECONNECTING: // POLLOUT in events, cli_data is cli_othernode
			pfd[pfdcount].events = POLLOUT;
			break;

		case TYPE_HEALTHCHECK: // cli_data empty, POLLOUT in events
			pfd[pfdcount].events = POLLOUT;
			clis[pfdcount].cli_data = NULL;
			return(pfdcount++);

		case TYPE_CONTROLCONN: // cli_data empty, POLLIN in events
		case TYPE_NOIDEA:
			pfd[pfdcount].events = POLLIN;
			clis[pfdcount].cli_data = NULL;
			return(pfdcount++);

		default:
			assert(1 == 2);
	}

	// OTHERNODE/OTHERNODEOUT/OTHERNODECONNECTING, the rest has return()ed above.
	clis[pfdcount].cli_data = malloc(sizeof(cli_othernode));
	if (clis[pfdcount].cli_data == NULL) {
		printf("[C%d] ERROR: malloc(): %m\n", fd);
		return(-1);
	}
	memset(clis[pfdcount].cli_data, 0, sizeof(cli_othernode));

	return(pfdcount++); // how does this work, eh? ;-)
}





static ssize_t find_other_node_cli(const char * const node_name, const size_t node_name_len) {
	ssize_t i;

	for (i = pfdcount - 1; i > 1; --i)
		if (IS_OTHERNODE(i)) {
			size_t tl = strlen(CLIDATA_OTHERNODE(i)->node_name);
			if (tl == node_name_len && !strncmp(CLIDATA_OTHERNODE(i)->node_name, node_name, node_name_len))
				return(i);
		}

	return(-1);
}


static void connect_unconnected_nodes() {
	// we could also keep number of otherclients in a var and number of othernodes connected in another var.
	// then this can just if (otherclientscnt != otherclientsconnected) return;
	char **c;

	for (c = config.other_nodes; *c != NULL; ++c) {
		ssize_t i = find_other_node_cli(*c, strlen(*c));

		if (i < 0) {
			//create sokkit, dont connect before i new_cli this
			struct addrinfo *dest = resolve(*c);
			if (dest != NULL) {
				int fd = socket_connectto(dest, 1);
				printf("[C%d] %s - connecting to node\n", fd, *c);
				freeaddrinfo(dest);
				if (fd >= 0) {
					i = new_cli(fd, TYPE_OTHERNODECONNECTING);
					if (i < 0) {
						close(fd);
						continue; // try next time
					}

					strcpy(clis[i].srchost, *c);

					CLIDATA_OTHERNODE(i)->node_name = strdup(*c);
					if (CLIDATA_OTHERNODE(i)->node_name == NULL) {
						cli_disconnect(i);
						continue;
					}
				}
			}
			else {
				printf("[C?] Failed to resolve %s\n", *c); // TODO: gaierror()
			}
		}
		else {
			if (clis[i].cli_type == TYPE_OTHERNODECONNECTING) {
				printf("[C%d] %s - connection timed out, retrying.\n", pfd[i].fd, *c);
				// we need to close this connection and retry it.
				// but we just need to close the fd and create a new one, no need to touch clis[] or pfd[].
				// preferalby start with int newfd = socket_connectto() because if this fails, we can cli_disconnect(i) (otherwise it'd close() some wrong fd)
				// if it succeeds, we close(clis[i].fd); clis[i].fd = newfd;
				struct addrinfo *dest = resolve(*c);
				if (dest != NULL) {
					int fd = socket_connectto(dest, 1);
					printf("[C%d] %s - connecting to node\n", fd, *c);
					freeaddrinfo(dest);
					if (fd >= 0) {
						close(pfd[i].fd);
						pfd[i].fd = fd;
					}
					else {
						cli_disconnect(i);
						continue;
					}
				}
				else {
					printf("[C?] Failed to resolve %s\n", *c); // TODO: gaierror()
					cli_disconnect(i);
					continue;
				}
			}
		}
	}
}

static void ping_other_nodes(const time_t now) {
	for (ssize_t i = pfdcount - 1; i > 1; --i)
		if (IS_OTHERNODECONNECTED(i)) {
			if (CLIDATA_OTHERNODE(i)->lasttransmission + config.node_ping_timeout <= now) {
				if (CLIDATA_OTHERNODE(i)->sent_ping) {
					// disconnect. ping timeout.
					printf("[C%d] %s ERROR: ping timeout. disconnecting.\n", pfd[i].fd, clis[i].srchost);
					cli_disconnect(i);
				}
				else {
					printf("[C%d] %s: pinging node.\n", pfd[i].fd, clis[i].srchost);
					switch (cli_send(i, "PING\n", strlen("PING\n"))) {
						case 0:
							CLIDATA_OTHERNODE(i)->sent_ping = 1;
							CLIDATA_OTHERNODE(i)->lasttransmission = now;
							break;

						case 1:
							pfd[i].events |= POLLOUT;
							CLIDATA_OTHERNODE(i)->sent_ping = 1;
							CLIDATA_OTHERNODE(i)->lasttransmission = now;
							break;

						case -1:
						default:
							printf("[C%d] %s ERROR: ping_other_nodes(): cli_send(): %m\n", pfd[i].fd, clis[i].srchost);
							cli_disconnect(i);
					}
				}
			}

		}

}

static void ask_nodes_for_bwstats() {
	size_t i;
	char *req = "BWSTATS\n";

	for (i = pfdcount - 1; i > 1; --i)
		if (IS_OTHERNODE(i) && !CLIDATA_OTHERNODE(i)->sent_bwstats && CLIDATA_OTHERNODE(i)->state != NODE_INBWSTATS) {
			uint64_t *bs = realloc(CLIDATA_OTHERNODE(i)->bwstats, sizeof(uint64_t) * destcount);
			if (bs != NULL) {
				CLIDATA_OTHERNODE(i)->bwstats = bs;
				memset(bs, 0, sizeof(uint64_t) * destcount);
				printf("[C%d] %s: ask_nodes_for_bwstats() - sending BWSTATS (req_in_flight: %d)\n", pfd[i].fd, clis[i].srchost, bwstat_requests_in_flight);
				switch (cli_send(i, req, strlen(req))) {
					case 0:
						CLIDATA_OTHERNODE(i)->sent_bwstats = 1;
						++bwstat_requests_in_flight;
						break;

					case 1:
						pfd[i].events |= POLLOUT;
						CLIDATA_OTHERNODE(i)->sent_bwstats = 1;
						++bwstat_requests_in_flight;
						break;

					case -1:
					default:
						printf("[C%d] %s ERROR: ask_nodes_for_bwstats(): cli_send(): %m\n", pfd[i].fd, clis[i].srchost);
						cli_disconnect(i);
				}
			}
			else {
				printf("[C%d] %s ERROR: ask_nodes_for_bwstats(): realloc(): %m\n", pfd[i].fd, clis[i].srchost);
			}
		}
}

static int cli_sendnodegreeting(const ssize_t cli) {
	int greetlen = NODE_GREETING_LEN + 1 + strlen(config.node_name) + 1;
	char greet[greetlen];

	memcpy(greet, NODE_GREETING, NODE_GREETING_LEN);
	greet[NODE_GREETING_LEN] = ' ';
	memcpy(greet + NODE_GREETING_LEN + 1, config.node_name, greetlen - NODE_GREETING_LEN - 1);
	greet[greetlen - 1] = '\n';

	switch (cli_send(cli, greet, greetlen)) {
		case 0:
			break;

		case 1:
			pfd[cli].events |= POLLOUT;
			break;

		case -1:
			return(-1);
	}
	return(0);
}

#define FORWARD_SRCS_I(x) (forward_srcs + (MAXADDRLEN * x))
static int append_forward_fd(const int fd, const struct sockaddr_storage *src) {
	if (forward_fd_count == forward_fd_size) {
		int *nfd = realloc(forward_fds, (forward_fd_size + 16) * sizeof(int));
		if (nfd == NULL)
			return(-1);

		forward_fds = nfd;

		char *nsrc = realloc(forward_srcs, (forward_fd_size + 16) * MAXADDRLEN);
		if (nsrc == NULL)
			return(-1);

		forward_srcs = nsrc;
		forward_fd_size += 16;
	}

	forward_fds[forward_fd_count] = fd;
	sockaddr_to_char(FORWARD_SRCS_I(forward_fd_count), (struct sockaddr *)src);
	forward_fd_count++;

	return(0);
}


static void connect_forwarded_fds() {
	uint64_t destloads[destcount];

	get_bandwidths(destloads);

	int i;
	for (i = 2; i < pfdcount; ++i)
		if (IS_OTHERNODECONNECTED(i) && CLIDATA_OTHERNODE(i)->state != NODE_INBWSTATS && !CLIDATA_OTHERNODE(i)->sent_bwstats) { // if it's a node and we're not downloading bwstats from it, we can include it here
			int d;
			for (d = 0; d < destcount; ++d)
				destloads[d] += CLIDATA_OTHERNODE(i)->bwstats[d];
		}

	printf("Current destloads:\n");
	int lowdest = -1;
	for (i = 0; i < destcount; ++i) {
		printf("- %s: %lu%s%s\n", destinations[i].host, destloads[i], (destinations[i].unhealthy ? " (UNHEALTHY)" : ""), (destinations[i].disabled ? " (DISABLED)" : ""));
		if (!destinations[i].unhealthy && !destinations[i].disabled) {
			if (lowdest < 0)
				lowdest = i;
			else if (destloads[i] < destloads[lowdest])
				lowdest = i;
		}
	}
	if (lowdest < 0 /* || destinations[lowdest].unhealthy || destinations[lowdest].disabled */) {
		printf("No healthy+enabled destination found. Disconnecting queue.\n");
		// disconnect all to be forwarded, we dont have a healthy destination.
		// remember: those are not clis, they're not in pfds and clis.
		for (i = 0; i < forward_fd_count; ++i)
			close(forward_fds[i]);

		goto cleanup;
	}

	for (i = 0; i < forward_fd_count; ++i) {
		printf("Connecting fd %d to destination %d\n", forward_fds[i], lowdest);
		new_forwarded_connection(forward_fds[i], FORWARD_SRCS_I(i), lowdest, config.dest_connection_timeout);
	}

	cleanup:
	forward_fd_count = 0;
	if (forward_fd_size > 16) {
		int *nfd = realloc(forward_fds, 16 * sizeof(int));
		if (nfd != NULL) {
			forward_fds = nfd;
			forward_fd_size = 16;
			char *nsrc = realloc(forward_srcs, 16 * MAXADDRLEN);
			if (nsrc != NULL)
				forward_srcs = nsrc;
		}
	}
}


static void check_unhealthy_dests() {
	ssize_t i;
	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC, &tp);

	for (i = 0; i < destcount; ++i)
		if (destinations[i].unhealthy && destinations[i].last_health_check + config.unhealthy_checks_interval <= tp.tv_sec) {
			ssize_t c;
			for (c = pfdcount - 1; c > 1; --c)
				if (clis[c].cli_type == TYPE_HEALTHCHECK && clis[c].cli_data == &(destinations[i])) {
					int fd = socket_connectto(destinations[i].desthost, 1);
					if (fd >= 0) {
						printf("[C%d] Destination %s healthcheck failed (timeout), retrying.\n", pfd[c].fd, destinations[i].host);
						close(pfd[c].fd);
						pfd[c].fd = fd;
						printf("[C%d] Running healthcheck for %s.\n", fd, destinations[i].host);
					} // if it fails, we'll retry later
					break;
				}

			if (c == 1) { // not found
				int fd = socket_connectto(destinations[i].desthost, 1);
				if (fd >= 0) {
					c = new_cli(fd, TYPE_HEALTHCHECK);
					if (c < 0) {
						close(fd);
						continue; // try next time
					}

					strcpy(clis[c].srchost, destinations[i].host);
					clis[c].cli_data = &(destinations[i]);
					printf("[C%d] Running healthcheck for %s.\n", fd, destinations[i].host);
				} // again, if it fails, we'll retry later.
			}
		}
}

#define ISCMD(x) (cmdlen == strlen(x) && !strncmp(start, x, strlen(x))) /* strlen() should be substitured compile-time anyway */
#define ISCMDPARAMS(x) (cmdlen > strlen(x) + 1 && !strncmp(start, x " ", strlen(x) + 1)) /* strlen() should be substitured compile-time anyway */
static int main_loop(int listensock, int controlsock) {
	clis[0].cli_type = TYPE_LISTEN;
	clis[0].cli_data = NULL;
	clis[1].cli_type = TYPE_CONTROLLISTEN;
	clis[1].cli_data = NULL;

	pfd[0].fd = listensock;
	pfd[0].events = POLLIN;
	pfd[1].fd = controlsock;
	pfd[1].events = POLLIN;

	struct timespec	now;
	// clock_gettime(CLOCK_MONOTONIC, &now);

	while (1) {
		clock_gettime(CLOCK_MONOTONIC, &now);
		int slp;
		if (next_maintenance_checks > now.tv_sec) {
			slp = (next_maintenance_checks - now.tv_sec) * 1000 + (1000000000 - now.tv_nsec) / 1000000;
		}
		else {
			ping_other_nodes(now.tv_sec);
			connect_unconnected_nodes();
			check_unhealthy_dests();
			next_maintenance_checks = now.tv_sec + config.maintenance_checks_interval;
			slp = config.maintenance_checks_interval * 1000;
		}

		if (next_connection_forward_time > 0) {
			if (next_connection_forward_time > now.tv_sec) {
				int slp2 = (next_connection_forward_time - now.tv_sec) * 1000 + (1000000000 - now.tv_nsec) / 1000000;
				if (slp2 < slp)
					slp = slp2;
			}
			else {
				next_connection_forward_time = 0;

				connect_forwarded_fds();
			}
		}

		int x = poll(pfd, pfdcount, slp);
		if (x < 0) {
			if (errno == EINTR)
				continue;

			printf("[??] ERROR: poll(): %m\n");
			assert(1 == 2);
		}

		if (pfd[0].revents & POLLIN) { // listen sock.
			if (forward_fd_count == 0) {
				ask_nodes_for_bwstats();
				if (bwstat_requests_in_flight > 0) {
					clock_gettime(CLOCK_MONOTONIC, &now);
					next_connection_forward_time = now.tv_sec + config.bwstats_max_delay;
				}
				else {
					next_connection_forward_time = 1;
				}
			}
			struct sockaddr_storage src;
			//memset(&src, 0, sizeof(struct sockaddr));
			socklen_t srclen = sizeof(struct sockaddr_storage);
			int fd = accept(pfd[0].fd, (struct sockaddr *)&src, &srclen);

			if (unlikely(fd < 0)) {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					printf("[??] ERROR: accept(): %m\n");
					assert(1 == 2);
				}
			}
			else {
				if (append_forward_fd(fd, &src) < 0) {
					printf("[??] ERROR: append_forward_fd(): %m\n");
					close(fd);
				}
			}

			pfd[0].revents ^= POLLIN;
		}


		if (pfd[1].revents & POLLIN) { // control listen sock
			struct sockaddr_storage src;
			socklen_t	srclen = sizeof(struct sockaddr_storage);

			int clifd = accept(pfd[1].fd, (struct sockaddr *)&src, &srclen);
			if (clifd < 0) {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					printf("[C?] ERROR: accept(): %m\n");
					assert(1 == 2);
				}
			}
			else {
				socket_setnonblock(clifd);
				int clin = new_cli(clifd, TYPE_CONTROLCONN);
				if (clin < 0) {
					close(clifd);
				}
				else {
					sockaddr_to_char(clis[clin].srchost, (struct sockaddr *)&src);
					printf("[C%d] %s - new connection\n", clifd, clis[clin].srchost);
				}
			}
			pfd[1].revents ^= POLLIN;
		}


		for (int i = 2; i < pfdcount; ++i) { // various control socks (with clients)
			if (pfd[i].revents & POLLIN) {
				if (clis[i].rbufpos == clis[i].rbufsize) {
					if (clis[i].rbufsize >= MAXINCOMINGBUF) {
						printf("[C%d] %s ERROR: refusing to alloc additional buffer beyond %d bytes\n", pfd[i].fd, clis[i].srchost, MAXINCOMINGBUF);
						cli_disconnect(i--);
						goto loop_end;
					}

					char *nb = realloc(clis[i].rbuf, clis[i].rbufsize + BUFCHUNK);
					if (nb == NULL) {
						printf("[C%d] %s ERROR: realloc(): %m\n", pfd[i].fd, clis[i].srchost);
						cli_disconnect(i--);
						goto loop_end;
					}

					clis[i].rbuf = nb;
					clis[i].rbufsize += BUFCHUNK;
				}

				int l = recv(pfd[i].fd, clis[i].rbuf + clis[i].rbufpos, clis[i].rbufsize - clis[i].rbufpos, 0);
				if (l <= 0) {
					if (l < 0)
						printf("[C%d] %s ERROR: recv(): %m\n", pfd[i].fd, clis[i].srchost);
					else
						printf("[C%d] %s disconnected\n", pfd[i].fd, clis[i].srchost);

					cli_disconnect(i--);
					goto loop_end;
				}

				if (IS_OTHERNODECONNECTED(i)) {
					clock_gettime(CLOCK_MONOTONIC, &now);
					CLIDATA_OTHERNODE(i)->lasttransmission = now.tv_sec;
				}

				// ================== process cmd =======================
				char *start = clis[i].rbuf;
				char *nl = memchr(start + clis[i].rbufpos, '\n', l);
				clis[i].rbufpos += l;

				if (nl != NULL)
					while (1) {
						printf("[C%d] got %.*s\n", pfd[i].fd, nl - start, start);
						size_t cmdlen = nl - start;
						size_t replen;
						char *rep;

						if (IS_OTHERNODECONNECTED(i) && CLIDATA_OTHERNODE(i)->state == NODE_INBWSTATS) {
							if (cmdlen == 1 && *start == '.') {
								CLIDATA_OTHERNODE(i)->state = NODE_IDLE;
								if (--bwstat_requests_in_flight == 0)
									next_connection_forward_time = 1;
							}
							else {
								char *space = memrchr(start, ' ', cmdlen);
								if (space == NULL) {
									printf("[C%d] ERROR: a node (%s) sent BWSTATS reply we don't understand: %.*s\n", pfd[i].fd, CLIDATA_OTHERNODE(i)->node_name, cmdlen, start);
									cli_disconnect(i--);
									goto loop_end;
								}
								//*space = 0;
								ssize_t destnr = destination_get_index(start, space - start);
								if (destnr >= 0) {
									*nl = 0;
									CLIDATA_OTHERNODE(i)->bwstats[destnr] += atoll(space + 1);
								}
								else
									printf("[C%d] a node (%s) sent BWSTATS reply for destination we don't know: %.*s\n", pfd[i].fd, CLIDATA_OTHERNODE(i)->node_name, space - start, start);
							}
							rep = NULL;
						}
						else {

							if (ISCMD("PONG")) {
								if (IS_OTHERNODECONNECTED(i))
									CLIDATA_OTHERNODE(i)->sent_ping = 0;
								rep = NULL;
							}
							else if (ISCMD("BWSTAT") || ISCMD("BWSTATS"))
								rep = get_bandwidths_str(&replen);
							else if (ISCMD("STAT") || ISCMD("STATS")) // list connections
								rep = get_conns_str(&replen);
							else if (ISCMDPARAMS(NODE_GREETING)) {
								// TODO: check if this thing is a host listed in othernodes. if not, just disconnect. kind of whitelisting by node names. bear in mind this does NOT check if the actual host this is connecting from is whitelisted. we can have NSA present themselves as whatever we have in our "network" setting.

								// let's check if we already opened connection to this one. if yes, we're dropping this one
								// if not, we actually just assign that connection a NODE type
								int clinr = find_other_node_cli(start + NODE_GREETING_LEN + 1, cmdlen - NODE_GREETING_LEN - 1);
								//printf("OTHERNODE '%.*s': %d\n", cmdlen - NODE_GREETING_LEN - 1, start + NODE_GREETING_LEN + 1, clinr);
								if (clinr < 0) {
									if (clis[i].cli_type != TYPE_CONTROLCONN) {
										// trying to auth as something else. can't happen. disconneting.
										printf("[C%d] ERROR: a node (%s) tried to reauthenticate as %.*s\n", pfd[i].fd, CLIDATA_OTHERNODE(i)->node_name, cmdlen - NODE_GREETING_LEN - 1, start + NODE_GREETING_LEN + 1);
										cli_disconnect(i--);
										goto loop_end;
									}

									clis[i].cli_type = TYPE_OTHERNODE;

									clis[i].cli_data = malloc(sizeof(cli_othernode));
									if (clis[i].cli_data == NULL) {
										printf("[C%d] %s ERROR: malloc(): %m\n", pfd[i].fd, clis[i].srchost);
										cli_disconnect(i--);
										goto loop_end;

									}
									memset(clis[i].cli_data, 0, sizeof(cli_othernode));

									CLIDATA_OTHERNODE(i)->node_name = malloc(cmdlen - NODE_GREETING_LEN);
									if (CLIDATA_OTHERNODE(i)->node_name == NULL) {
										printf("[C%d] %s ERROR: malloc(): %m\n", pfd[i].fd, clis[i].srchost);
										cli_disconnect(i--);
										goto loop_end;
									}
									memcpy(CLIDATA_OTHERNODE(i)->node_name, start + NODE_GREETING_LEN + 1, cmdlen - NODE_GREETING_LEN);
									CLIDATA_OTHERNODE(i)->node_name[cmdlen - NODE_GREETING_LEN - 1] = 0;
									clock_gettime(CLOCK_MONOTONIC, &now);
									CLIDATA_OTHERNODE(i)->lasttransmission = now.tv_sec;

									printf("[C%d] accepted other node: %s\n", pfd[i].fd, CLIDATA_OTHERNODE(i)->node_name);
								}
								else if (clinr == i) {
									// ignore, tried to reauthenticate for some reason
									printf("[C%d] tried to reauthenticate as %s\n", pfd[i].fd, CLIDATA_OTHERNODE(i)->node_name);
								}
								else {
									printf("[C%d] ERROR: a new node tried to authenticate as %s, but we already have a connection to it ([C%d]) - disconnected\n", pfd[i].fd, CLIDATA_OTHERNODE(clinr)->node_name, pfd[clinr].fd);
									cli_disconnect(i--);
									goto loop_end;
								}

								rep = NULL;
							}
							else if (ISCMDPARAMS("KICK") || ISCMDPARAMS("DISCONNECT")) { // disconnect connection by number (close cli socket? or how?)
								*nl = 0;

								uint64_t num = 1;
								for (size_t x = (*start == 'K' ? 5 : 11); x < cmdlen; ++x)
									if (start[x] < '0' || start[x] > '9') {
										num = 0;
										break;
									}

								if (num) {
									num = atoll(start + (*start == 'K' ? 5 : 11));
									printf("Closing connection " UINT64TPRINTF "\n", num);
									shutdown_connection_nr(num);
								}
								else {
									// TODO: gotta search dests/srcs
								}
								rep = NULL;
							}
							else if (ISCMDPARAMS("DISABLE")) { // disable dest by name
								ssize_t destnr = destination_get_index(start + 8, cmdlen - 8);
								if (destnr >= 0) {
									destinations[destnr].disabled = 1;
									printf("Disabled destination %s\n", destinations[destnr].host);
								}

								rep = NULL;
							}
							else if (ISCMDPARAMS("ENABLE")) { // "un-disable" dest by name
								ssize_t destnr = destination_get_index(start + 7, cmdlen - 7);
								if (destnr >= 0) {
									destinations[destnr].disabled = 0;
									printf("Re-enabled destination %s\n", destinations[destnr].host);
								}

								rep = NULL;
							}
							else if (ISCMDPARAMS("HEALTHY") || ISCMDPARAMS("MARKHEALTHY")) {
								ssize_t destnr = destination_get_index(start + (*start == 'H' ? 8 : 12), cmdlen - (*start == 'H' ? 8 : 12));
								if (destnr >= 0) {
									destinations[destnr].unhealthy = 0;
									printf("Marked destination %s as healthy\n", destinations[destnr].host);
								}

								rep = NULL;
							}
							else if (ISCMDPARAMS("UNHEALTHY") || ISCMDPARAMS("MARKUNHEALTHY")) {
								ssize_t destnr = destination_get_index(start + (*start == 'U' ? 10 : 14), cmdlen - (*start == 'U' ? 10 : 14));
								if (destnr >= 0) {
									destinations[destnr].unhealthy = 1;
									printf("Marked destination %s as unhealthy\n", destinations[destnr].host);
								}

								rep = NULL;
							}
							else if (ISCMD("DESTS") || ISCMD("LISTDESTS") || ISCMD("DESTINATIONS") || ISCMD("LISTDESTINATIONS")) {
								rep = get_dests_str(&replen);
							}
							else if (ISCMD("QUIT") || ISCMD("EXIT")) {
								printf("[C%d] %s disconnected (%.*s)\n", pfd[i].fd, clis[i].srchost, cmdlen, start);
								cli_disconnect(i--);
								goto loop_end;
							}
							else if (ISCMD("+BWSTATS")) {
								if (IS_OTHERNODECONNECTED(i) /* && CLIDATA_OTHERNODE(i)->sent_bwstats -- no need to verify this, but to be 100% strict, we can */) {
									CLIDATA_OTHERNODE(i)->state = NODE_INBWSTATS;
									CLIDATA_OTHERNODE(i)->sent_bwstats = 0;
								}
								rep = NULL;
							}
							else if (ISCMD("PING")) {
								switch (cli_send(i, "PONG\n", strlen("PONG\n"))) {
									case 0:
										break;

									case 1:
										pfd[i].events |= POLLOUT;
										break;

									case -1:
										printf("[C%d] ERROR (%s): cli_send(): %m\n", pfd[i].fd, clis[i].srchost);
										cli_disconnect(i--);
										goto loop_end;
								}
								rep = NULL;
							}
							else
								rep = NULL;
							// reload config? gonna be troublesome with destinations (pointer in forwarded conndata)
							// TODO: list control connections: LISTCLI? just LIST? CLIS?
						}

						if (rep != NULL) {
							switch (cli_send(i, rep, replen)) {
								case 0:
									break;

								case 1:
									pfd[i].events |= POLLOUT;
									break;

								case -1:
									printf("[C%d] ERROR (%s): cli_send(): %m\n", pfd[i].fd, clis[i].srchost);
									free(rep); // since we're jumping out
									cli_disconnect(i--);
									goto loop_end;
							}
							free(rep);
						}

						start = nl + 1; // look for another nl starting right after the previous one
						nl = memchr(start, '\n', clis[i].rbufpos - (start - clis[i].rbuf));
						if (nl == NULL) {
							clis[i].rbufpos -= (start - clis[i].rbuf);
							memmove(clis[i].rbuf, start, clis[i].rbufpos);
							break;
						}
					}
				// =============== /process cmd ==============
				pfd[i].revents ^= POLLIN;
			}


			if (pfd[i].revents & POLLOUT) {
				if (clis[i].cli_type == TYPE_HEALTHCHECK) {
					int se;
					socklen_t selen = sizeof(se);
					getsockopt(pfd[i].fd, SOL_SOCKET, SO_ERROR, &se, &selen);
					printf("[C%d] connect() returned: %d (%s)\n", pfd[i].fd, se, strerror(se));
					if (se == 0) {
						printf("[C%d] Destination %s connection successful, marking as healthy again.\n", pfd[i].fd, ((lbdest *)(clis[i].cli_data))->host);
						((lbdest *)(clis[i].cli_data))->unhealthy = 0;
					}
					// it's just a healthcheck, we disconnect anyway
					cli_disconnect(i--);
					goto loop_end;
				}
				else if (clis[i].cli_type == TYPE_OTHERNODECONNECTING) {
					int se;
					socklen_t selen = sizeof(se);
					getsockopt(pfd[i].fd, SOL_SOCKET, SO_ERROR, &se, &selen);
					printf("[C%d] connect() returned: %d (%s)\n", pfd[i].fd, se, strerror(se));
					if (se != 0) {
						cli_disconnect(i--);
						goto loop_end;
					}
					pfd[i].events |= POLLIN;
					pfd[i].events ^= POLLOUT;

					printf("[C%d] sending authentication as %s to %s\n", pfd[i].fd, config.node_name, clis[i].srchost);
					if (cli_sendnodegreeting(i) < 0) {
							printf("[C%d] ERROR (%s): cli_sendnodegreeting(): %m\n", pfd[i].fd, clis[i].srchost);
							cli_disconnect(i--);
							goto loop_end;
					}

					clis[i].cli_type = TYPE_OTHERNODEOUT;

					pfd[i].revents ^= POLLOUT;
				}
				else {
					switch (cli_flush(i)) {
						case 0:
							break;

						case 1:
							pfd[i].events ^= POLLOUT;
							break;

						case -1:
							printf("[C%d] ERROR (%s): cli_flush(): %m\n", pfd[i].fd, clis[i].srchost);
							cli_disconnect(i--);
							goto loop_end;
					}
					pfd[i].revents ^= POLLOUT;
				}

			}

			loop_end: // we jump here if we finished processing current client (especially if we disconnect it) and want to go to another.
			;
		}

	}
}












int load_config() {
	configdata	*cd = read_config("bwlb.conf");
	configdata	*c;

	if (cd == NULL)
		return(-1);

	// some defaults:
	config.node_ping_timeout = 15;
	config.bwstats_max_delay = 5;
	config.maintenance_checks_interval = 15;
	config.unhealthy_checks_interval = config.maintenance_checks_interval;
	config.dest_connection_timeout = 5;

	for (c = cd; c != NULL; c = c->next) {
		if (!strcmp(c->key, "node_name")) {
			if (config.node_name != NULL)
				free(config.node_name);
			config.node_name = c->value;
			printf("(config) node_name: %s\n", config.node_name);
			c->value = NULL;
		}
		else if (!strcmp(c->key, "node_ping_timeout")) {
			config.node_ping_timeout = atoi(c->value);
			if (config.node_ping_timeout < 1) {
				printf("(config) node_ping_timeout can't be below 1 (%d), aborting.\n", config.node_ping_timeout);
				free_configdata(cd);
				return(-1);
			}
			printf("(config) node_ping_timeout: %d\n", config.node_ping_timeout);
		}
		else if (!strcmp(c->key, "maintenance_checks_interval")) {
			config.maintenance_checks_interval = atoi(c->value);
			if (config.maintenance_checks_interval < 1) {
				printf("(config) maintenance_checks_interval can't be below 1 (%d), aborting.\n", config.maintenance_checks_interval);
				free_configdata(cd);
				return(-1);
			}
			printf("(config) maintenance_checks_interval: %d\n", config.maintenance_checks_interval);
		}
		else if (!strcmp(c->key, "dest_connection_timeout")) {
			config.dest_connection_timeout = atoi(c->value);
			if (config.dest_connection_timeout < 1) {
				printf("(config) dest_connection_timeout can't be below 1 (%d), aborting.\n", config.dest_connection_timeout);
				free_configdata(cd);
				return(-1);
			}
			printf("(config) dest_connection_timeout: %d\n", config.dest_connection_timeout);
		}
		else if (!strcmp(c->key, "bwstats_max_delay")) {
			config.bwstats_max_delay = atoi(c->value);
			if (config.bwstats_max_delay < 1) {
				printf("(config) bwstats_max_delay can't be below 1 (%d), aborting.\n", config.bwstats_max_delay);
				free_configdata(cd);
				return(-1);
			}
			printf("(config) bwstats_max_delay: %d\n", config.bwstats_max_delay);
		}
		else if (!strcmp(c->key, "unhealthy_checks_interval")) {
			config.unhealthy_checks_interval = atoi(c->value);
			if (config.unhealthy_checks_interval < 1) {
				printf("(config) unhealthy_checks_interval can't be below 1 (%d), aborting.\n", config.unhealthy_checks_interval);
				free_configdata(cd);
				return(-1);
			}
			printf("(config) unhealthy_checks_interval: %d\n", config.unhealthy_checks_interval);
		}
		else if (!strcmp(c->key, "listen_addr")) {
			if (config.listen_addr != NULL)
				free(config.listen_addr);
			config.listen_addr = c->value;
			printf("(config) listen_addr: %s\n", config.listen_addr);
			c->value = NULL;
		}
		else if (!strcmp(c->key, "control_addr")) {
			if (config.control_addr != NULL)
				free(config.control_addr);
			config.control_addr = c->value;
			printf("(config) control_addr: %s\n", config.control_addr);
			c->value = NULL;
		}
		else if (!strcmp(c->key, "network")) {
			if (config.other_nodes != NULL) {
				free(config.other_nodes[0]); // those are strtok'd, so only the first one is an actual malloc()ed region
				free(config.other_nodes);
			}

			char *x = c->value;
			int cnt = 0;
			do {
				++cnt;
				x = strchr(x + 1, ',');
			} while (x != NULL);
			++cnt;

			config.other_nodes = malloc(sizeof(char *) * cnt);
			char *t;
			for (cnt = 0, t = strtok_r(c->value, ",", &x); t != NULL; t = strtok_r(NULL, ",", &x), ++cnt) {
				config.other_nodes[cnt] = t;
				printf("(config) + other_node: %s\n", t);
			}

			config.other_nodes[cnt++] = NULL;

			c->value = NULL;
		}
		else if (!strcmp(c->key, "destinations")) {
			// TODO: should we actually resolve this here, or like with other node addresses, when connecting? right now I can't have a "dyndns" destination, cause it's resolved only once.
			// TODO: doing this when we have connections open will break stuff. we're keeping a pointer to destinations[x] in conndata (and now in clis[] too, for TYPE_HEALTHCHECK); but we load config only when starting for now.
			if (destcount > 0) {
				for (int i = 0; i < destcount; ++i) {
					freeaddrinfo(destinations[i].desthost);
				}

				free(destinations[0].host);

				free(destinations);
				destinations = NULL;
				destcount = 0;
			}

			char *x = c->value;
			int cnt = 0;
			do {
				++cnt;
				x = strchr(x + 1, ',');
			} while (x != NULL);

			destinations = malloc(sizeof(lbdest) * cnt);
			assert(destinations != NULL);
			memset(destinations, 0, sizeof(lbdest) * cnt);

			char *t;
			int i;
			for (i = 0, t = strtok_r(c->value, ",", &x); t != NULL; t = strtok_r(NULL, ",", &x)) {
				struct addrinfo *ai = resolve(t);
				if (ai != NULL) {
					destinations[i].desthost = ai;
					destinations[i].host = t;
					printf("(config) + destination: %s\n", t);
					++i;
				}
				else {
					printf("(config) cannot resolve destination lb host: %s\n", t);
					free_configdata(cd);
					return(-1);
				}
			}
			//if (i != cnt) {
				// realloc destinations? there's fewer than we thought, cause we couldn't resolve some address. - we abort now when this happens, makes more sense.
			//}
			destcount = cnt;
			c->value = NULL;
		}
	}
	free_configdata(cd);

	// now let's try to see if there's config.node_name in config.othernodes
	if (config.node_name == NULL) {
		printf("(config) node_name not defined.\n");
		abort();
	}

	if (config.other_nodes != NULL) {
		char **c = config.other_nodes;
		char **rewrite = NULL;
		while (*c != NULL) {
			if (!strcmp(*c, config.node_name)) {
				if (rewrite == NULL)
					rewrite = c;
				printf("(config) removing self from other_nodes: %s\n", *c);
				//free(*c); - NO! they're all one char* in config.other_nodes[0], they're just pointers inside the same string, as it's strtok'd.
			}
			else {
				if (rewrite != NULL) {
					*rewrite = *c;
					++rewrite;
				}
			}
			++c;
		}
		if (rewrite != NULL)
			*rewrite = NULL;
	}

	return(0);
}

int main(const int argc, const char *argv[]) {
	memset(&config, 0, sizeof(bwlbconfig));

	if (load_config() < 0)
		abort();

	pfd = malloc(sizeof(struct pollfd) * pfdsize);
	assert(pfd != NULL);
	clis = malloc(sizeof(clidescr) * pfdsize);
	assert(clis != NULL);

	int controlsock = socket_listenon(config.control_addr, 1);
	assert(controlsock >= 0);
	int listensock = socket_listenon(config.listen_addr, 1);
	assert(listensock >= 0);
	printf("++ Bound listen socket to %s\n", config.listen_addr);
	printf("++ Bound control socket to %s\n", config.control_addr);
	printf("++ Destination count: %d\n", destcount);

	signal(SIGPIPE, SIG_IGN);

	main_loop(listensock, controlsock);

//	for (int x = 0; x < destcount; ++x)
//		freeaddrinfo(destinations[x].desthost);
// TODO: free config
//	free(destinations);
	free(pfd);

	for (ssize_t i = pfdcount; i > 1; --i)
		cli_disconnect(i);

	free(clis);

	return(0);
}
